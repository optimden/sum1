<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login</title>
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
</head>
<body>
    <div class="body">
        <div class="main">
            <h3>Вход для менеджеров</h3>
            @if (isset($error))
                <p>{{ $error }}</p>
            @endif
            <form method="POST">
                {{ csrf_field() }}
                <div class="login_form">
                    <p>Email:</p>
                    <input type="email" name="email">
                    <p>Пароль:</p>
                    <input type="password" name="password">
                    <button type="submit">Отправить</button>
                </div>
            </form>
        </div>
    </div>
</body>
</html>