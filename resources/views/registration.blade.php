<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registration</title>
    <link rel="stylesheet" href="{{ asset('css/registration.css') }}">
</head>
<body>
    <div class="body">
        <div class="main">
            <h3>Регистрация</h3>
            @if (isset($errors))
                @foreach ($errors as $error)
                    <p>{{ $error }}</p>
                @endforeach
            @endif
            @if (isset($error))
                <p>{{ $error }}</p>
            @endif
            <form method="POST" action="/registration">
                {{ csrf_field() }}
                <div class="registration_form">
                    <p>Логин:</p>
                    <input type="text" name="name">
                    <p>Email:</p>
                    <input type="text" name="email">
                    <p>Пароль:</p>
                    <input type="password" name="password">
                    <p>Подтверждение пароля:</p>
                    <input type="password" name="password_confirmation">
                    <button type="submit">Зарегистрироваться</button>
                </div>
            </form>
        </div>
    </div>
</body>
</html>