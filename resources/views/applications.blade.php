<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Applications</title>
    <link rel="stylesheet" href="{{ asset('css/applications.css') }}">
    <script src="{{ asset('js/change_application.js') }}"></script>
    <script src="{{ asset('js/filter_applications.js') }}"></script>
</head>
<body>
    <div class="body">
        <div class="main">
            <div class="navigation">
                <h4>Здравствуйте, {{ $user->name }}!</h4>
                <a href="/logout"><button>Выйти</button></a>
                <a href="/statistics"><button>Статистика</button></a>
            </div>
            <div class="options">
                <table>
                    <tr><th>Поиск</th><th>Дата подачи заявки</th><th>Город</th><th>Сортировка</th></tr>
                    <tr>
                        <td class="search">
                            @if (isset($params["search"]))
                                <input type="text" name="search" placeholder="Фамилия или телефон..." value="{{ $params['search'] }}">
                            @else
                                <input type="text" name="search" placeholder="Фамилия или телефон...">
                            @endif
                        </td>
                        <td class="filter_date">
                            С:
                            @if (isset($params["start_date"]))
                                <input type="text" name="start_date" placeholder="dd.mm.yy" value="{{ $params['start_date'] }}">
                            @else
                                <input type="text" name="start_date" placeholder="dd.mm.yy">
                            @endif
                            До:
                            @if (isset($params["end_date"]))
                                <input type="text" name="end_date" placeholder="dd.mm.yy" value="{{ $params['end_date'] }}">
                            @else
                                <input type="text" name="end_date" placeholder="dd.mm.yy">
                            @endif
                        </td>
                        <td class="filter_city">
                            <select name="city">
                            <option>Выбрано: {{ $city_count }}</option>
                            <option value="city_clear" name="city_clear">---</option>
                            @foreach ($cities as $index => $city)
                                @if (in_array($city, $params))
                                    <option value="{{ $city }}" name="city{{ $index }}" data-selected="true">{{ $city }}</option>
                                @else
                                    <option value="{{ $city }}" name="city{{ $index }}">{{ $city }}</option>
                                @endif
                            @endforeach
                            </select>
                        </td>
                        <td class="sort">
                            <select name="sort_type" size="1">
                            <option value="---">---</option>
                            @if (isset($params["sort_type"]))
                                @if ($params["sort_type"] == "created_at")
                                    <option value="created_at" selected>Дата</option>
                                    <option value="lastname">Фамилия</option>
                                @else
                                    <option value="created_at">Дата</option>
                                    <option value="lastname" selected>Фамилия</option>
                                @endif
                            @else
                                <option value="created_at">Дата</option>
                                <option value="lastname">Фамилия</option>
                            @endif
                            </select>
                            @if (isset($params["sort_type"]))
                                <select name="sort_order" size="1">
                                @if (isset($params["sort_order"]))
                                    @if ($params["sort_order"] == "asc")
                                        <option value="asc" selected>По возрастанию</option>
                                        <option value="desc">По убыванию</option>
                                    @else
                                        <option value="asc">По возрастанию</option>
                                        <option value="desc" selected>По убыванию</option>
                                    @endif
                                @else
                                    <option value="asc" selected>По возрастанию</option>
                                    <option value="desc">По убыванию</option>
                                @endif
                            @else
                                <select name="sort_order" size="1" disabled="true">
                                <option value="asc" selected>По возрастанию</option>
                            @endif
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="applications">
                @foreach ($applications as $application)
                    <div class="application application_show">
                        <p data-name="name">Имя: {{ $application->name }}</p>
                        <p data-name="lastname">Фамилия: {{ $application->lastname }}</p>
                        <p data-name="patronym">Отчество: {{ $application->patronym }}</p>
                        <p data-name="address">Адрес: {{ $application->address }}</p>
                        <p data-name="phone">Номер телефона: {{ $application->phone }}</p>
                        <p data-name="email">Email: {{ $application->email }}</p>
                        <p data-name="created_at">Создана: {{ $application->created_at }}</p>
                        <button class="edit_button">Редактировать</button>
                    </div>
                    <div class="application_form" data-id="{{ $application->id }}">
                        <label>Имя:
                        <input type="text" name="name" value="{{ $application->name }}">
                        </label>
                        <label>Фамилия:
                        <input type="text" name="lastname" value="{{ $application->lastname }}">
                        </label>
                        <label>Отчество:
                        <input type="text" name="patronym" value="{{ $application->patronym }}">
                        </label>
                        <label>Адрес:
                        <input type="text" name="address" value="{{ $application->address }}">
                        </label>
                        <label>Номер телефона:
                        <input type="text"  name="phone" value="{{ $application->phone }}">
                        </label>
                        <label>Email:
                        <input type="text"  name="email" value="{{ $application->email }}">
                        </label>
                        <button class="accept_button">Подтвердить</button>
                    </div>
                @endforeach
            </div>
            <div class="pagination">
                <ul>
                    @if (count($pages) > 1)
                        @for ($i = 1; $i < count($pages) + 1; $i++)
                            @if ($applications->currentPage() == $i)
                                <li><b>{{ $i }}</b></li>
                            @else
                                <li><a href="{{ $pages[$i] }}">{{ $i }}</a></li>
                            @endif
                        @endfor
                    @endif
                </ul>
            </div>
        </div>
    </div>
</body>
</html>