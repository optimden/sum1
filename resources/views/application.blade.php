<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Application</title>
    <link rel="stylesheet" href="{{ asset('css/application.css') }}">
    <script src="{{ asset('js/application_input_mask.js') }}"></script>
</head>
<body>
    <div class="body">
        <div class="main">
            @if (!isset($message))
                <h3>Заполните данные заявки:</h3>
                <form method="POST">
                    {{ csrf_field() }}
                    <div class="application_form">
                        <p>Имя:</p>
                        <input type="text" name="name" required>
                        <p>Фамилия:</p>
                        <input type="text" name="lastname" required>
                        <p>Отчество:</p>
                        <input type="text" name="patronym" required>
                        <p>Адрес:</p>
                        <input type="text" name="address" id="address" placeholder="Город, район, улица, дом, квартира" required>
                        <p>Номер телефона:</p>
                        <input type="text" name="phone" id="phone" placeholder="+7(xxx)xxx-xx-xx" required>
                        <p>Email:</p>
                        <input type="email" name="email" required>
                        <button type="submit">Отправить</button>
                    </div>
                </form>
            @else
                <div class="message">
                    <h3>{{ $message }}</h3>
                </div>
            @endif
        </div>
    </div>
</body>
</html>