<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Statistics</title>
    <link rel="stylesheet" href="{{ asset('css/statistics.css') }}">
    <script src="{{ asset('js/get_statistics.js') }}"></script>
</head>
<body>
    <div class="body">
        <div class="main">
            <div class="panel">
                <p>Здравствуйте, {{ $user->name }}!</p>
                <a href="/applications"><button>Заявки</button></a>
            </div>
            <div class="graph">
                <h3>Статистика количества заявок</h3>
                <canvas id="graph"></canvas>
            </div>
        </div>
    </div>
</body>
</html>