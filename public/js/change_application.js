function show_application_form(event){
    // Получаю заявку со статичными данными
    var application = event.target.parentNode;
    // Получаю форму изменения заявки
    var application_form = application.nextElementSibling;
    // Скрываю статичную заявку, показываю форму ее изменения
    application.classList.toggle("application_show");
    application_form.classList.toggle("application_form_show");
}

function send_ajax(application, application_form) {
    // Получаю массив измененных полей
    var changed_fields = get_changed_fields(application, application_form);
    // Проверяю, было ли что-то изменено
    if (changed_fields.size > 0) {
        var xhr = new XMLHttpRequest();
        var url = "/application";
        xhr.open("PUT", url);
        // Формирую тело PUT-запроса
        var data = "";
        for (input of changed_fields) {
            data += encodeURIComponent(input[0]) + "=" + encodeURIComponent(input[1]) + "&";
        }
        // Удаляю лишний амперсанд в конце
        data = data.slice(0, -1);
        // Из мета тега страницы получаю csrf-токен
        var csrf = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
        // Выставляю заголовки PUT-запроса
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.setRequestHeader("X-CSRF-TOKEN", csrf);
        // Прописываю коллбек функцию для ajax-запроса
        xhr.onreadystatechange = function() {
            if ((xhr.readyState == 4) && (xhr.status == 200)) {
                // Получаю HTML обновленного узла
                var updated = xhr.responseText;
                if (updated == "0") {
                    // Если данные были успешно заменены, обновляю данные заявки на клиенте
                    update_fields(application, changed_fields);
                }
            }
        }
        xhr.send(data);
    }
}

function update_fields(application, changed_fields) {
    // Заменяю значения в статичной заявке на новые значения из формы обновления заявки
    changed_fields.delete("id");
    for (let field of changed_fields) {
        var p = application.querySelector("p[data-name=" + field[0] +"]");
        p.textContent = p.textContent.split(": ")[0] + ": " + field[1];
    }
}

function get_changed_fields(application, application_form) {
    // Проверяем, какие данные заявки были изменены
    // Если изменения были, получаем также id заявки из атрибута data-id
    // Получаю исходные значение полей
    var original_values = new Map();
    for (row of application.querySelectorAll("p")) {
        var name = row.getAttribute("data-name");
        var row_fields = row.textContent.split(": ");
        var value = row_fields[row_fields.length - 1];
        original_values.set(name, value);
    }
    // Получаю измененные значения полей
    var changed_values = new Map();
    for (input of application_form.querySelectorAll("input")) {
        changed_values.set(input.getAttribute("name"), input.value);
    }
    // Смотрю, какие поля были изменены
    var diff = new Map();
    for (input of changed_values) {
        var key = input[0];
        var value = input[1];
        if (original_values.get(key) != value) {
            diff.set(key, value);
        }
    }
    // Если изменения есть, то добавляю к массиву id измененной заявки
    if (diff.size > 0) {
        diff.set("id", application_form.dataset.id);
    }
    return diff;
}

function accept_changes(event){
    // Получаю форму изменения заявки
    var application_form = event.target.parentNode;
    // Получаю заявку со статичными данными
    var application = application_form.previousElementSibling;
    // Отправляю ajax-запрос (если есть изменения)
    send_ajax(application, application_form);
    // Скрываю форму изменения заявки, показываю саму статичную заявку
    application.classList.toggle("application_show");
    application_form.classList.toggle("application_form_show");
}

function main() {
    // Получаю кнопки изменения каждой заявки
    var edit_buttons = document.getElementsByClassName("edit_button");
    // Назначаю обработчик события на каждую кнопку
    for (let button of edit_buttons) {
        button.addEventListener("click", show_application_form);
    }
    // Получаю кнопки подтверждения изменений заявок
    var accept_buttons = document.getElementsByClassName("accept_button");
    // Назначаю обработчик события на эти кнопки
    for (let button of accept_buttons) {
        button.addEventListener("click", accept_changes);
    }
}

// Запускаю основной скрипт после того, как страница будет полностью загружена
document.addEventListener("DOMContentLoaded", main);