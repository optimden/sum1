function draw_graph(points) {
    let canvas = document.getElementById("graph");
    setup_graph(canvas, points);
}

function setup_graph(canvas, points) {
    // Получаю коэффициент перевода абстрактных размеров в реальные
    var dpr = window.devicePixelRatio || 1;
    // Пересчитываю размеры canvas в реальных пикселях устройства
    var canvas_rect = canvas.getBoundingClientRect();
    canvas.width = canvas_rect.width * dpr;
    canvas.height = canvas_rect.height * dpr;
    var graph = canvas.getContext('2d');
    // Получаем ширину и высоту canvas
    var canvas_width = canvas.width;
    var canvas_height = canvas.height;

    var x_width = canvas_width * 0.5;
    var y_height = canvas_height * 0.7;

    // Задаю параметры шрифта
    var font_size = 0.05 * y_height;
    graph.font = font_size + "px Arial";

    var x_shift = 2.1 * font_size + canvas_width * 0.2;
    var y_shift = canvas_height * 0.05;

    graph.fillStyle = "black"; // Задаём чёрный цвет для линий 
    graph.lineWidth = 0.005 * y_height; // Ширина линии
    graph.beginPath(); // Запускает путь
    graph.moveTo(x_shift, y_shift); // Указываем начальный путь
    graph.lineTo(x_shift, y_shift + y_height); // Перемешаем указатель
    graph.lineTo(x_shift + x_width, y_shift + y_height); // Ещё раз перемещаем указатель
    graph.stroke(); // Делаем контур

    // Рисуем деления по оси y
    // Сперва получаем максимальное значение присланных заявок
    var max_count = Math.max(...Array.from(points.values()));
    var y_step = max_count / 10;
    for (let i = 0; i < 11; i++) {
        graph.fillText(
            ((10 - i) * y_step).toFixed(1), 
            x_shift - 2 * font_size, 
            y_shift + (i * y_height)/10
        );
        graph.beginPath();
        graph.moveTo(x_shift - 0.4 * font_size, y_shift + (i * y_height)/10);
        graph.lineTo(x_shift + 0.4 * font_size, y_shift + (i * y_height)/10);
        graph.stroke();
    }

    // Прописываем метки - даты по оси x
    // И сразу рисуем гистограммы для каждой даты
    var x_step = x_width / points.size;
    var i = 0;
    for (date of points) {
        graph.fillStyle = "black";
        graph.fillText(
            date[0], 
            x_shift + x_step * (i + 0.25), 
            y_shift + 1.4* font_size + y_height
        );
        graph.fillStyle = "steelblue";
        graph.fillRect(
            x_shift + x_step * (i + 0.25), 
            y_shift + y_height - graph.lineWidth / 2, 
            x_step / 2, 
            -(date[1] * y_height) / max_count
        );
        i++;
    }
}

function get_statistics() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/get_statistics_ajax");
    xhr.onreadystatechange = function() {
        if ((xhr.status == 200) && (xhr.readyState == 4)) {
            var date = JSON.parse(xhr.responseText);
            if (date != 500) {
                let points = new Map();
                for (day of date) {
                    points.set(day["date"], day["count"]);
                }
                draw_graph(points); 
            }
        }
    }
    xhr.send();
}

document.addEventListener("DOMContentLoaded", get_statistics);