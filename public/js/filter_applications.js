function filter_main() {
    // Получаю все элементы, связанные с
    // поиском, фильтрацией, сортировкой
    var search = document.querySelector(".search>input");
    var date_inputs = document.querySelectorAll(".filter_date>input");
    var city = document.querySelector(".filter_city>select");
    var sort = document.querySelector(".sort>select[name='sort_type']");
    var sort_order = document.querySelector(".sort>select[name='sort_order']");
    // Назначаю обработчики событий на полученные элементы
    search.addEventListener("change", apply_search);
    for (date_input of date_inputs) {
        date_input.addEventListener("change", filter_by_date);
        date_input.addEventListener("keydown", filter_by_date);
    }
    city.addEventListener("change", filter_by_city);
    sort.addEventListener("change", apply_sort);
    sort_order.addEventListener("change", apply_sort_order);
}
    
function send_query(name, value) {
    var url = window.location.href;
    // Получаем список названий url-параметров в текущем запросе
    // чтобы, если надо, убрать дублирование параметров
    var search = window.location.search;
    var names = [];
    if (search != "") {
        search = search.matchAll(/[?&]([^=&]+)=/g);
        for (match of search) {
            names.push(match[1]);
        }
    }
    url = url.replace(/([?&]page=\w+$)|(page=\w+(?:&|$))/, "");
    // Проверяем, был ли до этого сделан какой-то запрос
    if (!url.includes("?")) {
        // Никаких фильтров применено не было
        url += "?" + name + "=" + value;
    } else {
        // В запросе уже есть параметры
        if (names.includes(name)) {
            var pattern = new RegExp(name + "=[^&]+", "g");
            url = url.replace(pattern, name + "=" + value);
        } else {
            url += "&" + name + "=" + value;
        }
    }
    window.location.href = url;
}

function send_cutting_query(name) {
    // Удаляет из запроса пару имя=значение по данному имени
    var url = window.location.href;
    url = url.replace(/([?&]page=\w+$)|(page=\w+(?:&|$))/, "");
    var regexp = new RegExp("(" +name + "=[0-9.]+(?:&|$))|([?&]" + name + "=[^&]+$)");
    window.location.href = url.replace(regexp, "");
}

function apply_search(event) {
    var value = event.target.value;
    var name = event.target.getAttribute("name");
    if (value == "") {
        send_cutting_query(name);
    } else {
        send_query(name, value);
    }
}

function filter_by_date(event) {
    // Получаю значение введенной даты
    var value = event.target.value;
    var key = event.keyCode;
    // Если был введен очередной символ
    if (event.type == "keydown") {
        // И была нажата любая клавиша, кроме Enter, Backspace, Delete
        // и стрелок
        if (![8, 13, 37, 38, 39, 40, 46].includes(key)) {
            // Не печатаю введенный символ, если длина строки 8 символов
            // или символ - не число
            event.preventDefault();
            if ((value.length < 8) && ((47 < key) && (key < 58))) {
                // После каждых двух введенных символов добавляю точку
                var new_value = value + event.key;
                if ((new_value.search(/(\d{2})$/) != -1) && (new_value.length < 8)) {
                    new_value += ".";
                }
                event.target.value = new_value;
            }
        } else if (key == 13){
            // Если введенный символ - Enter и строка короче 7 символов
            // запрещаю отправку запроса с неполной датой
            if ((0 < value.length) && (value.length < 8)) {
                event.preventDefault();
            } else if (value.length == 8) {
                var name = event.target.getAttribute("name");
                send_query(name, value);
            } else if (value.length == 0){
                var name = event.target.getAttribute("name");
                send_cutting_query(name);
            }
        }
    } else if (value.length == 8) {
        // Если пользователь подтвердил ввод, кликнув за пределы input
        var name = event.target.getAttribute("name");
        if (value == "") {
            send_cutting_query(name);
        } else {
            send_query(name, value);
        }
    }
}

function filter_by_city(event) {
    var value = event.target.value;
    var option = event.target.querySelector("option[value='" + value + "']");
    var name = option.getAttribute("name");
    if (value == "city_clear") {
        var url = window.location.href;
        var regexp = /(city\d+=[^&]+(?:&|$))|([?&]city\d+=[^&]+$)/g;
        window.location.href = url.replace(/([?&]page=\w+$)|(page=\w+(?:&|$))/, "").replace(regexp, "").replace(/[?&]$/, "");
    } else {
        if (option.getAttribute("data-selected")) {
            send_cutting_query(name);
        } else {
            send_query(name, value);
        }
    }
}

function apply_sort(event) {
    var value = event.target.value;
    var sort_order = document.querySelector(".sort>select[name='sort_order']");
    if (value != "---") {
        // Применяю сортировку. Порядок сортировки по умолчанию - по возрастанию
        sort_order.setAttribute("disabled", "false");
        var name = event.target.getAttribute("name");
        send_query(name, value);
    } else {
        // Сбрасываю сортировку
        sort_order.setAttribute("disabled", "true");
        var url = window.location.href;
        var regexp = /([?&]sort_\w+=[^&]+$)|(sort_\w+=[^&]+(?:&|$))/;
        url = url.replace(/([?&]page=\d+$)|(page=\d+(?:&|$))/, "");
        console.log(url);
        window.location.href = url.replace(regexp, "").replace(regexp, "");
    }
}

function apply_sort_order(event) {
    var value = event.target.value;
    var name = event.target.getAttribute("name");
    send_query(name, value);
}

document.addEventListener("DOMContentLoaded", filter_main);