<?php

namespace App\Http\Controllers;

use App\Models\Applications;
use Illuminate\Support\Facades\Route;

// /
Route::get('/', function () {
    return redirect()->route('application');
});

// /application
Route::get('/application', [ApplicationController::class, 'show_page'])->name('application');
Route::post('/application', [ApplicationController::class, 'create_application']);
Route::put('/application', [ApplicationController::class, 'change_application_ajax']);

// applications
Route::get('/applications', [ApplicationsController::class, 'show_applications'])
        ->name('applications')
        ->middleware('auth');

// login/logout
Route::get('/login', [ManagerLoginController::class, 'show_login_form'])->name("login");
Route::post('/login', [ManagerLoginController::class, 'login']);
Route::get('/logout', [ManagerLoginController::class, 'logout']);

// registration
Route::get('/registration', [RegistrationController::class, 'show_registration_form']);
Route::post('/registration', [RegistrationController::class, 'registrate']);

// statistics
Route::get('/statistics', [StatisticsController::class, 'show_statistics'])
        ->middleware('auth');
Route::get('/get_statistics_ajax', [StatisticsController::class, 'get_statistics_ajax'])
        ->middleware('auth');
