<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Applications;
use Illuminate\Support\Facades\Auth;
use Exception;

class StatisticsController extends Controller {

    public function show_statistics(Request $request) {
        return view("statistics", ["user" => Auth::user()]);
    }

    public function get_statistics_ajax(Request $request) {
        try {
            $applications = Applications::get_statistics();
        } catch (Exception $e) {
            return 500;
        }

        // Сериализуем данные статистики в JSON-формат
        return $applications->toJson(JSON_PRETTY_PRINT);
    }
}
