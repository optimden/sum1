<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Applications;
use Exception;

class ApplicationsController extends Controller
{
    public function show_applications(Request $request) {
        // Получаю url-параметры запроса, если есть
        $params = $request->query();

        // Получаю список заявок
        try {
            $applications = Applications::get_applications($params);
        } catch (Exception $e) {
            if ($e->getCode() == 22008) {
                // Если получаю ошибку Datetime field overflow
                return back();
            }
            abort(500);
        }

        // Получаю массив страниц пагинации
        $pages = $applications->getUrlRange(1, $applications->lastPage());

        // Получаю список городов, для фильтрации заявок клиентом
        try {
            $cities_models = Applications::get_cities_list();
        } catch (Exception $e) {
            abort(500);
        }
        
        $cities = [];
        foreach ($cities_models as $city) {
            $cities[] = $city->city;
        }

        // Получаю количество выбранных пользователем городов
        $city_count = count(preg_grep("/city\d+/", array_keys($params)));

        return view("applications", [
                    "applications" => $applications,
                    "pages" => $pages,
                    "cities" => $cities,
                    "city_count" => $city_count,
                    "params" => $params,
                    "user" => Auth::user()
        ]);
    }
}
