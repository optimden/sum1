<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use Exception;

class RegistrationController extends Controller {
    
    public function show_registration_form(Request $request) {
        return view("registration");
    }

    public function registrate(Request $request) {
        $validator =  Validator::make($request->input(), [
            "name" => "required",
            "email" => ["required", "email"],
            "password" => ["required", "confirmed"]
        ]);

        if ($validator->fails()) {
            return redirect("/registration")
                        ->withErrors($validator)
                        ->withInput();
        }

        $fields = $validator->validate();
        $password = $fields["password"];
        $fields["password"] = Hash::make($password);

        try {
            $user = User::create($fields);
        } catch (Exception $e) {
            if ($e->getCode() == 23505) {
                // Если нарушено ограничение уникальности
                return view("registration", [
                    "error" => "Пользователь с таким email уже существует."
                ]);
            } else {
                abort(500);
            }
        }

        if (Auth::attempt(["email" => $user->email, "password" => $password])) {
            return redirect()->route("applications");
        }

        abort(500);
    }
}
