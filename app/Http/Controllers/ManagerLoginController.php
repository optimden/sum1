<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

use Illuminate\Support\Facades\Auth;

use Exception;

class ManagerLoginController extends Controller {

    public function show_login_form(Request $request) {
        return view("login");
    }

    public function login(Request $request) {
        $credentials = $request->only("email", "password");

        if (Auth::attempt($credentials)) {
            return redirect()->route("applications");
        }

        return view("login", [
            "error" => "Invalid login/password pair."
        ]);
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
    }
}
