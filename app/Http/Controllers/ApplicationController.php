<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Applications;
use Exception;

class ApplicationController extends Controller
{
    public function show_page(Request $request) {
        return view("application");
    }

    public function create_application(Request $request) {
        $fields = $request->input();

        try {
            Applications::create($fields);
        } catch (Exception $e){
            if ($e->getCode() == 23505) {
                // Если получили ошибку уникальности, сообщаем
                // про это пользователю
                return view("application", 
                            ["message" => "Пользователь с таким email или телефоном уже существует."]
                        );
            } else {
                // Иначе - пишем пользователю, что произошла внутренняя ошибка
                return view("application", ["message" => "Извините, что-то пошло не так. Попробуйте снова."]);
            }
        }
        return view("application", ["message" => "Заявка успешно создана."]);
    }

    public function change_application_ajax(Request $request) {
        $fields = $request->post();

        try {
            $id = $fields["id"];
            unset($fields["id"]);
            Applications::where("id", $id)
                            ->update($fields);
        } catch (Exception $e) {
            return 500;
        }
        return 0;
    }
}
