<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Applications extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function get_applications(array $params) {
        if (empty($params)) {
            return self::paginate(5);
        }
        $query = new Applications();
        
        // Добавляем к запросу поиск по фамилии или номеру телефона
        if (isset($params["search"])) {
            $search = $params["search"];
            // Если пользователь ищет по фамилии
            if (preg_match("/[:alpha:а-яА-Я\s]+/", $search)) {
                $query = $query->whereRaw("LOWER(lastname) LIKE LOWER(?)", ["%" . $search . "%"]);
            } else {
                // Если пользователь ищет по номеру телефона
                $query = $query->where("phone", "LIKE", "%{$search}%");
            }
        }
        // Добавляем фильтрацию по дате
        if (isset($params["start_date"])) {
            $start_date = array_reverse(explode(".", $params["start_date"]));
            $start_date[0] = "20" . $start_date[0]; 
            $query = $query->where("created_at", ">", implode("-", $start_date));
        }
        if (isset($params["end_date"])) {
            $end_date = array_reverse(explode(".", $params["end_date"]));
            $end_date[0] = "20" . $end_date[0];
            $query = $query->where("created_at", "<", $params["end_date"]);
        }
        // Добавляем фильтрацию по городам
        // Получаем список названий городов из url-параметров
        $cities = array_values(array_filter(
            $params, 
            fn($key) => (preg_match("/city\d+/", $key)) ? true : false, 
            ARRAY_FILTER_USE_KEY
        ));
        if (!empty($cities)) {
            $cities_string = "('" . implode("', '", $cities) . "')";
            $query = $query->whereRaw(
                "SUBSTRING(address, ?) IN " . $cities_string,
                ["[^,]+"]
            );
        }

        // Добавляем сортировку
        // Чтобы на фронтенде не делать метод отправки поискового запроса
        // для нескольких новых параметров сразу, в случае добавления
        // пользователем сортировки по умолчанию сортируем по возрастанию
        if (isset($params["sort_type"])) {
            $sort_order = isset($params["sort_order"]) ? $params["sort_order"] : "asc";
            $query = $query->orderBy($params["sort_type"], $sort_order);
        }
        return $query->paginate(5)->withQueryString();
    }

    public static function get_cities_list() {
        return self::selectRaw('SUBSTRING(address, ?) AS city', ["^[^,]+"])
                        ->groupBy('city')
                        ->get();
    }

    public static function get_statistics() {
        return self::selectRaw('DATE(created_at) AS date, COUNT(created_at)')
                    ->groupBy('date')
                    ->orderBy('date', 'asc')
                    ->get();
    }
}
